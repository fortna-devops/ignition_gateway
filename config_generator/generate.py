#!/usr/bin/env python


import argparse
import json
import pathlib
import logging

from modbus import generate_modbus_config
from tags import generate_tags
from transaction_groups import generate_transaction_groups

logger = logging.getLogger()

LOGGING_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'


def generate(input_file: pathlib.Path, output_dir: pathlib.Path):
    with open(input_file, 'r') as fp:
        config = json.load(fp)

    output_dir = output_dir/input_file.stem
    logger.debug('Create config dir')
    output_dir.mkdir(parents=True, exist_ok=True)

    logger.info('Generate ModBus configs')
    for device in config['modbus_devices']:
        modbus_path = output_dir/'modbus'
        modbus_path.mkdir(parents=True, exist_ok=True)
        with open(modbus_path/f'{device["name"]}_{device["address"]}:{device["port"]}.csv', 'w') as fp:
            generate_modbus_config(fp, device['units'])

    logger.info('Generate tags')
    with open(output_dir/'tags.json', 'w') as fp:
        tags = generate_tags(fp, config['assets'], config['modbus_devices'])

    logger.info('Generate transaction groups')
    with open(output_dir/'transaction_groups.xml', 'wb') as fp:
        generate_transaction_groups(fp, tags)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=pathlib.Path, help='Site definition file to process.')
    parser.add_argument('--output-dir', '-o', type=pathlib.Path, default=pathlib.Path('./ignition_configs'),
                        help='Directory to store ignition config files.')
    parser.add_argument('--log-level', default=logging.INFO, choices=logging._levelToName.values(),
                        help='Logging level.')
    args = vars(parser.parse_args())
    logging.basicConfig(level=args.pop('log_level'), format=LOGGING_FORMAT)
    generate(**args)
