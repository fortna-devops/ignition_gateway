import ctypes

__all__ = ['Register']


class Register:
    _type_map = {
        ctypes.c_int16: 'HoldingRegister',
        ctypes.c_uint16: 'HoldingRegisterUInt16'
    }

    def __init__(self, address: str, c_type, scale: float, name: str, units: str):
        self.address = address
        self.c_type = c_type
        self.scale = scale
        self.name = name
        self.units = units

    def __repr__(self):
        return f'<register.Register address: {self.address} c_type: {self.c_type.__name__}>'

    @property
    def ignition_type(self):
        return self._type_map[self.c_type]

    def is_consecutive(self, prev: 'Register') -> bool:
        return int(self.address) - int(prev.address) == 1 and self.c_type == prev.c_type
