import ctypes
from typing import ClassVar, Tuple, Dict, Type, List, Iterator

from register import Register


__all__ = ['UNIT_TYPE_MAP', 'BaseUnitType', 'QM42VT2UnitType', 'M12FTH3QUnitType']


class BaseUnitType:
    registers: ClassVar[Tuple[Register, ...]]

    @classmethod
    def consecutive_registers(cls) -> Iterator[List[Register]]:
        sequence = []
        prev_register = None
        for register in cls.registers:
            if prev_register and not register.is_consecutive(prev_register):
                yield sequence
                sequence = [register]
            else:
                sequence.append(register)
            prev_register = register
        yield sequence


class QM42VT2UnitType(BaseUnitType):
    registers = (
        Register('5200', ctypes.c_uint16, 10000., 'rms_velocity_z', 'in/sec'),
        Register('5201', ctypes.c_uint16, 10000., 'rms_velocity_z_metric', 'mm/sec'),
        Register('5202', ctypes.c_int16, 100., 'temperature', '°F'),
        Register('5203', ctypes.c_int16, 100., 'temperature_celsius', '°C'),
        Register('5204', ctypes.c_uint16, 10000., 'rms_velocity_x', 'in/sec'),
        Register('5205', ctypes.c_uint16, 10000., 'rms_velocity_x_metric', 'mm/sec'),
        Register('5206', ctypes.c_uint16, 1000., 'peak_acceleration_z', 'G'),
        Register('5207', ctypes.c_uint16, 1000., 'peak_acceleration_x', 'G'),
        Register('5208', ctypes.c_uint16, 10., 'peak_frequency_z', 'Hz'),
        Register('5209', ctypes.c_uint16, 10., 'peak_frequency_x', 'Hz'),
        Register('5210', ctypes.c_uint16, 1000., 'rms_acceleration_z', 'G'),
        Register('5211', ctypes.c_uint16, 1000., 'rms_acceleration_x', 'G'),
        Register('5212', ctypes.c_uint16, 1000., 'kurtosis_z', ''),
        Register('5213', ctypes.c_uint16, 1000., 'kurtosis_x', ''),
        Register('5214', ctypes.c_uint16, 1000., 'crest_acceleration_z', ''),
        Register('5215', ctypes.c_uint16, 1000., 'crest_acceleration_x', ''),
        Register('5216', ctypes.c_uint16, 10000., 'peak_velocity_z', 'in/sec'),
        Register('5217', ctypes.c_uint16, 10000., 'peak_velocity_z_metric', 'mm/sec'),
        Register('5218', ctypes.c_uint16, 10000., 'peak_velocity_x', 'in/sec'),
        Register('5219', ctypes.c_uint16, 10000., 'peak_velocity_x_metric', 'mm/sec'),
        Register('5220', ctypes.c_uint16, 1000., 'hf_rms_acceleration_z', 'G'),
        Register('5221', ctypes.c_uint16, 1000., 'hf_rms_acceleration_x', 'G'),
    )


class M12FTH3QUnitType(BaseUnitType):
    registers = (
        Register('0000', ctypes.c_uint16, 100., 'humidity', '%RH'),
        Register('0001', ctypes.c_int16, 20., 'temperature_metric', '°C'),
        Register('0002', ctypes.c_int16, 20., 'temperature', '°F'),
    )


UNIT_TYPE_MAP: Dict[str, Type[BaseUnitType]] = {
    'vt': QM42VT2UnitType,
    'ambient': M12FTH3QUnitType,
}
