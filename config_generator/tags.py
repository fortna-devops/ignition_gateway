import ctypes
import io
import json
from typing import Dict, Any, List

from units import UNIT_TYPE_MAP


__all__ = ['generate_tags']


def get_limits(c_int_type):
    signed = c_int_type(-1).value < c_int_type(0).value
    bit_size = ctypes.sizeof(c_int_type) * 8
    signed_limit = 2 ** (bit_size - 1)
    return (-signed_limit, signed_limit - 1) if signed else (0, 2 * signed_limit - 1)


def get_unit_type(devices: List[Dict[str, Any]], device_name: str, unit_id: int):
    for device in devices:
        for unit in device['units']:
            if device['name'] == device_name and unit['id'] == unit_id:
                return unit['type']
    raise ValueError(f"Can't find unit with device_name {device_name} and unit_id {unit_id}")


def get_misc_tags(asset_name: str, component_type: str) -> Dict[str, Any]:
    return {
        'name': 'misc',
        'tagType': 'Folder',
        'tags': [
            {
                'valueSource': 'memory',
                'accessRights': 'Read_Only',
                'dataType': 'String',
                'name': 'asset_name',
                'value': asset_name,
                'tagType': 'AtomicTag',
            },
            {
                'valueSource': 'memory',
                'accessRights': 'Read_Only',
                'dataType': 'String',
                'name': 'component_type',
                'value': component_type,
                'tagType': 'AtomicTag',
            }
        ],
    }


def get_modbus_device_tags(units: List[Dict[str, Any]], devices: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    device_tags = []
    for unit in units:
        unit_type_name = get_unit_type(devices, unit['device_name'], unit['id'])
        unit_type_cls = UNIT_TYPE_MAP[unit_type_name]
        register_tags = []
        for register_index, register in enumerate(unit_type_cls.registers, 1):
            raw_low, raw_high = get_limits(register.c_type)
            register_tags.append({
                'valueSource': 'opc',
                'accessRights': 'Read_Only',
                'formatString': '#,##0.####',
                'tagType': 'AtomicTag',
                'clampMode': 'No_Clamp',
                'scaleMode': 'Linear',
                'dataType': 'Float4',
                'opcServer': 'Ignition OPC UA Server',
                'opcItemPath': f'ns\u003d1;s\u003d[{unit["device_name"]}]{unit_type_name}{register_index}',
                'name': register.name,
                'rawLow': raw_low,
                'rawHigh': raw_high,
                'scaledLow': raw_low / register.scale,
                'scaledHigh': raw_high / register.scale,
                'engUnit': register.units
            })
        device_tags.append({
            'name': unit_type_name,
            'tagType': 'Folder',
            'tags': register_tags
        })
    return device_tags


def get_motor_tags(asset_name: str, component_type: str, units: List[Dict[str, Any]], devices: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    misc_tags = get_misc_tags(asset_name, component_type)
    plc_tags = {
        'name': 'plc',
        'tagType': 'Folder',
        'tags': [
            {
                'valueSource': 'memory',
                'accessRights': 'Read_Only',
                'dataType': 'Float4',
                'name': 'voltage',
                'formatString': '#,##0.####',
                'value': 0.0,
                'tagType': 'AtomicTag',
            },
            {
                'valueSource': 'memory',
                'accessRights': 'Read_Only',
                'dataType': 'Float4',
                'name': 'current',
                'formatString': '#,##0.####',
                'value': 0.0,
                'tagType': 'AtomicTag',
            },
            {
                'valueSource': 'memory',
                'accessRights': 'Read_Only',
                'dataType': 'Float4',
                'name': 'speed',
                'formatString': '#,##0.####',
                'value': 0.0,
                'tagType': 'AtomicTag',
            }
        ]
    }

    modbus_device_tags = get_modbus_device_tags(units, devices)
    return [misc_tags, plc_tags] + modbus_device_tags


def get_bearing_tags(asset_name: str, component_type: str, units: List[Dict[str, Any]], devices: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    misc_tags = get_misc_tags(asset_name, component_type)
    modbus_device_tags = get_modbus_device_tags(units, devices)
    return [misc_tags] + modbus_device_tags


def get_gearbox_tags(asset_name: str, component_type: str, units: List[Dict[str, Any]], devices: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    misc_tags = get_misc_tags(asset_name, component_type)
    modbus_device_tags = get_modbus_device_tags(units, devices)
    return [misc_tags] + modbus_device_tags


def get_chain_input_tags(asset_name: str, component_type: str, _units: List[Dict[str, Any]], _devices: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    misc_tags = get_misc_tags(asset_name, component_type)
    plc_tags = {
        'name': 'plc',
        'tagType': 'Folder',
        'tags': [
            {
                'valueSource': 'memory',
                'accessRights': 'Read_Only',
                'dataType': 'Float4',
                'name': 'left_tension',
                'formatString': '#,##0.####',
                'value': 0.0,
                'tagType': 'AtomicTag',
            },
            {
                'valueSource': 'memory',
                'accessRights': 'Read_Only',
                'dataType': 'Float4',
                'name': 'right_tension',
                'formatString': '#,##0.####',
                'value': 0.0,
                'tagType': 'AtomicTag',
            },
            {
                'valueSource': 'memory',
                'accessRights': 'Read_Only',
                'dataType': 'Float4',
                'name': 'temperature',
                'formatString': '#,##0.####',
                'value': 0.0,
                'tagType': 'AtomicTag',
            }
        ]
    }
    return [misc_tags, plc_tags]


def get_divert_tags(asset_name: str, component_type: str, _units: List[Dict[str, Any]], _devices: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    misc_tags = get_misc_tags(asset_name, component_type)
    plc_tags = {
        'name': 'plc',
        'tagType': 'Folder',
        'tags': [
            {
                'valueSource': 'memory',
                'accessRights': 'Read_Only',
                'dataType': 'Float4',
                'name': 'divert_cycles',
                'formatString': '#,##0.####',
                'value': 0.0,
                'tagType': 'AtomicTag',
            },
            {
                'valueSource': 'memory',
                'accessRights': 'Read_Only',
                'dataType': 'Float4',
                'name': 'failed_diverts',
                'formatString': '#,##0.####',
                'value': 0.0,
                'tagType': 'AtomicTag',
            },
            {
                'valueSource': 'memory',
                'accessRights': 'Read_Only',
                'dataType': 'Float4',
                'name': 'relative_strain',
                'formatString': '#,##0.####',
                'value': 0.0,
                'tagType': 'AtomicTag',
            }
        ]
    }
    return [misc_tags, plc_tags]


def get_feed_air_tags(asset_name: str, component_type: str, units: List[Dict[str, Any]], devices: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    misc_tags = get_misc_tags(asset_name, component_type)
    plc_tags = {
        'name': 'plc',
        'tagType': 'Folder',
        'tags': [
            {
                'valueSource': 'memory',
                'accessRights': 'Read_Only',
                'dataType': 'Float4',
                'name': 'input_pressure',
                'formatString': '#,##0.####',
                'value': 0.0,
                'tagType': 'AtomicTag',
            },
            {
                'valueSource': 'memory',
                'accessRights': 'Read_Only',
                'dataType': 'Float4',
                'name': 'output_pressure',
                'formatString': '#,##0.####',
                'value': 0.0,
                'tagType': 'AtomicTag',
            },
        ]
    }
    modbus_device_tags = get_modbus_device_tags(units, devices)
    return [misc_tags, plc_tags] + modbus_device_tags


COMPONENT_TAGS_MAP = {
    'motor': get_motor_tags,
    'bearing': get_bearing_tags,
    'gearbox': get_gearbox_tags,
    'chain': get_chain_input_tags,
    'divert': get_divert_tags,
    'feed_air': get_feed_air_tags,
}


def generate_tags(fp: io.TextIOWrapper, assets: List[Dict[str, Any]], devices: List[Dict[str, Any]]) -> Dict[str, Any]:
    tags = []
    for asset in assets:
        asset_name = asset['name']
        asset_tags = []

        for component in asset['components']:
            component_type = component['type']

            asset_tags.append({
                'name': component_type,
                'tagType': 'Folder',
                'tags': COMPONENT_TAGS_MAP[component_type](asset_name, component_type, component.get('units'), devices)
            })

        tags.append({
            'name': asset_name,
            'tagType': 'Folder',
            'tags': asset_tags
        })

    result = {
        'name': 'assets',
        'tagType': 'Folder',
        'tags': tags
    }
    json.dump(result, fp, indent=2)
    return result
