import csv
import io
from typing import List, Dict, Any

from units import UNIT_TYPE_MAP


__all__ = ['generate_modbus_config']


def generate_modbus_config(fp: io.TextIOWrapper, units: List[Dict[str, Any]]):
    fp.write('10\n')
    writer = csv.writer(fp)
    for unit in units:
        unit_type = UNIT_TYPE_MAP[unit['type']]
        counter = 1
        for seq in unit_type.consecutive_registers():
            writer.writerow([
                unit['type'],
                counter,
                counter + len(seq) - 1,
                'false',
                unit['id'],
                seq[0].ignition_type,
                seq[0].address
            ])
            counter += len(seq)
