import itertools
import io
from typing import Dict, Any

from lxml import etree


__all__ = ['generate_transaction_groups']


TG_TYPE_MAP = {
    'Float4': '4',
    'String': '7',
}


def get_tags_parts(tags, parts=()):
    if isinstance(tags, list):
        for i in tags:
            yield from get_tags_parts(i, parts)
    elif isinstance(tags, dict):
        parts += (tags['name'],)
        if 'tags' in tags:
            yield from get_tags_parts(tags['tags'], parts)
        else:
            yield parts, tags['dataType']


def generate_transaction_groups(fp: io.TextIOWrapper, tags: Dict[str, Any]):
    project = etree.Element('Project')
    groups = etree.SubElement(project, 'Groups')
    for (asset_name, component_type), grouped_tags in itertools.groupby(get_tags_parts(tags), lambda p: (p[0][1], p[0][2])):
        group_config = etree.SubElement(groups, 'GroupConfig', attrib={
            'typeKey': 'historical',
            'name': f'{asset_name}_{component_type}',
            'path': ''
        })
        gc_prop = etree.SubElement(group_config, 'Property', attrib={
            'name': 'CONFIGURED_ITEMS',
            'isComplex': 'true'
        })
        for tag_path, data_type in grouped_tags:
            joined_tags = '/'.join(tag_path)
            item_config = etree.SubElement(gc_prop, 'ItemConfig', attrib={
                'name': joined_tags,
                'typeId': '3'
            })
            etree.SubElement(item_config, 'Property', attrib={
                'name': 'TARGET_DATA_TYPE'
            }).text = TG_TYPE_MAP[data_type]
            etree.SubElement(item_config, 'Property', attrib={
                'name': 'DRIVING_TAG_PATH'
            }).text = f'[default]{joined_tags}'
            etree.SubElement(item_config, 'Property', attrib={
                'name': 'TARGET_NAME'
            }).text = tag_path[-1]
        etree.SubElement(group_config, 'Property', attrib={
            'name': 'INDEX_COLUMN'
        }).text = 'id'
        etree.SubElement(group_config, 'Property', attrib={
            'name': 'UPDATE_RATE'
        }).text = '10.0'
        etree.SubElement(group_config, 'Property', attrib={
            'name': 'TABLE_NAME'
        }).text = f'{component_type}_input_data'
        etree.SubElement(group_config, 'Property', attrib={
            'name': 'CUSTOM_INDEX_COLUMN'
        }).text = 'true'
        etree.SubElement(group_config, 'Property', attrib={
            'name': 'EXECUTION_ENABLED'
        }).text = 'true'
        etree.SubElement(group_config, 'Property', attrib={
            'name': 'DATA_SOURCE'
        }).text = 'local'
        etree.SubElement(group_config, 'Property', attrib={
            'name': 'TIMESTAMP_COLUMN'
        }).text = 'timestamp'

    fp.write(etree.tostring(project, pretty_print=True))
