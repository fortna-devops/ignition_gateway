#!/usr/bin/env python

import logging
from http import server, HTTPStatus

import configargparse

logger = logging.getLogger()

LOGGING_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'


class RequestHandler(server.BaseHTTPRequestHandler):

    def do_POST(self):
        self.send_response(HTTPStatus.OK)
        self.end_headers()


if __name__ == '__main__':
    parser = configargparse.ArgumentParser(default_config_files=['config.ini'], auto_env_var_prefix='data_api_')
    parser.add_argument('--host', default='0.0.0.0', help='Host to serve')
    parser.add_argument('--port', default=8888, type=int, help='Port to listen on')
    parser.add_argument('--log-level', default=logging.INFO, choices=logging._levelToName.values(),
                        help='Logging level')

    args = parser.parse_args()

    logging.basicConfig(level=args.log_level, format=LOGGING_FORMAT)

    httpd = server.HTTPServer((args.host, args.port), RequestHandler)
    try:
        httpd.serve_forever()
    finally:
        httpd.server_close()
