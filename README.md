# Ignition gateway setup

This repo is a POC of Ignition gateway setup

## Run Ignition demo in docker

### Pre-requirements
* Install [Docker](https://docs.docker.com/get-docker/)
* Install [docker-compose](https://docs.docker.com/compose/install/)

### Steps to run
* Create `GATEWAY_PASSWORD` file and put an ignition admin password (one line) there.
* Create `POSTGRES_PASSWORD` file and put a postgres admin password (one line) there.
* Run `docker-compose up -d --build`
* To see some logs run `docker-compose logs -f`

## Generate Ignition configuration files

### Pre-requirements
* Run `pip install -r requirements.txt`

### Input files
You need to prepare a json input file for config generator and put into [config_input](./config_input) dir.
There is an [example.json](./config_input/example.json) that can help you figure out the structure.

### Structure of json input file
There are two main sections `modbus_devices` and `assets`

#### ModBus devices
In this section you need to specify ModBus TCP devices that will be connected to Ignition.
Each device must have next fields:

* name - unique device name
* address - IP address or hostname of the device 
* port - network port (usually 502)
* units - list of connected units (sensors)

Each unit should be defined like this:

* id - ModBus RTU address
* type - type of the device

Currently we support two unit types:

* vt - for banner QM42VT2
* ambient - for banner M12FTH3Q

#### Assets
In this section you need to specify asset/components structure.

Each asset should have:

* name - Name of the asset (same as in cloud database)
* components - List of component definitions

Each component should have:

* type - Type of component

Currently we support next component types:

* motor
* bearing
* gearbox
* chain
* divert
* feed_air

Optionally each component could have `units` list.
Each unit should have next fields:

* device_name - ModBus device name specified in [modbus_devices](#modbus-devices) section.
* id - ModBus unit id specified in [modbus_devices](#modbus-devices) section.

### Generate config files
Run `python config_generator/generate.py config_input/example.json`
Where `config_input/example.json` is a path to your json input file.
As a result `ignition_configs/example` dir will be created and there you will find Ignition configuration files.

### Ignition configuration files
In the `modbus` dir you will find .csv files for Ingition device connection.
Each file represents one device. File name represents device settings.
For instance:
You have file `modbus_device_1_192.168.1.11:502.csv` that means:

* device name is `modbus_device_1`
* device address is `192.168.1.11`
* device port is `502`
* content of this file is a ModBus device registers configuration

`tags.json` contains tag configuration for Ignition project and should be imported to the `Tags` node in the Ignition Tag Browser.

`transaction_groups.xml` contains transaction groups configuration and should be imported to the Transaction Groups section in the Ignition Project Browser.
