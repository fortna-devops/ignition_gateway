#!/usr/bin/env python

import argparse
import enum
import json
import logging
import signal
import threading
import time
import io
from datetime import datetime, date
from typing import Union, List, Any

import requests
import configargparse
import sqlalchemy as sa
from sqlalchemy.engine.url import make_url
from sqlalchemy.orm import sessionmaker

logger = logging.getLogger()

LOGGING_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'


def _signal_handler(signum, _frame):
    logger.debug(f'Got signal {signum}')
    sender.stop()


for sig in (signal.SIGINT, signal.SIGTERM):
    signal.signal(sig, _signal_handler)


class SenderError(Exception):
    pass


class Encoder(json.JSONEncoder):

    def default(self, o: Any) -> str:  # pylint: disable=E0202
        if isinstance(o, enum.Enum):
            return o.name
        if isinstance(o, datetime):
            return o.strftime('%Y-%m-%d %H:%M:%S.%f')
        if isinstance(o, date):
            return o.strftime('%Y-%m-%d')
        return super(Encoder, self).default(o)


class Sender:

    _shutdown_event: threading.Event = threading.Event()

    def __init__(self, database_url: str, database_user: str, database_password: Union[str, io.TextIOWrapper],
                 site_name: str, table_name: str, order_by_field: str, index_field: str, batch_size: int, cloud_url: str,
                 send_interval: int, error_delay: int, leftover_delay: int):
        db_url = make_url(database_url)
        db_url.username = database_user
        db_url.password = database_password if isinstance(database_password, str) else database_password.readline()
        engine = sa.create_engine(db_url)
        meta = sa.MetaData(bind=engine)
        meta.reflect(only=(table_name,))
        self._site_name = site_name
        self._table = sa.Table(table_name, meta)
        self._session_cls = sessionmaker(bind=engine)
        self._order_by_field = order_by_field
        self._index_field = index_field
        self._batch_size = batch_size
        self._cloud_url = cloud_url
        self._send_interval = send_interval
        self._error_delay = error_delay
        self._leftover_delay = leftover_delay

    def _process(self) -> int:
        session = self._session_cls()
        try:
            logger.debug('Fetch data')
            result = session.execute(self._table.select().order_by(self._order_by_field))
            rows = result.fetchmany(self._batch_size)
            if not rows:
                logger.warning(f'No data in {self._table.name}')
                return self._send_interval

            keys = result.keys()
            row_count = result.rowcount
            data = []
            ids = []
            for row in rows:
                data.append(row.values())
                ids.append(row[self._index_field])

            logger.debug('Send data')
            self._send_data(keys, data)
            logger.debug('Delete data')
            session.execute(self._table.delete().where(getattr(self._table.c, self._index_field).in_(ids)))
            session.commit()
        except Exception:
            logger.exception('An error occurred. Rollback.')
            session.rollback()
            return self._error_delay
        else:
            logger.info(f'{len(rows)} rows processed')
            leftover = row_count - len(rows)
            if leftover > 0:
                logger.info(f'{leftover} rows left')
                return self._leftover_delay
            return self._send_interval
        finally:
            session.close()

    def _send_data(self, keys: List[str], data: List[Any]):
        response = requests.post(self._cloud_url, data=json.dumps({
            'site': self._site_name,
            'table_name': self._table.name,
            'keys': keys,
            'data': data
        }, cls=Encoder))
        logger.debug(f'Server response: {response}')
        if response.status_code != requests.codes.ok:
            raise SenderError(response.text)

    def start(self):
        logger.info('Starting...')
        last_call = 0
        delay = self._send_interval
        while not self._shutdown_event.is_set():
            time.sleep(.01)
            now = time.time()
            if now - last_call <= delay:
                continue

            last_call = now
            delay = self._process()

    def stop(self):
        logger.info('Shutting down...')
        self._shutdown_event.set()


if __name__ == '__main__':
    parser = configargparse.ArgumentParser(default_config_files=['config.ini'], auto_env_var_prefix='data_sender_')
    parser.add_argument('--database-url', help='Database connection url in format '
                                               '"dialect[+driver]://host/dbname[?key=value..]"')
    parser.add_argument('--database-user', help='Database user')
    pwd_group = parser.add_mutually_exclusive_group()
    pwd_group.add_argument('--database-password', help='Database password')
    pwd_group.add_argument('--database-password-file', dest='database_password', type=argparse.FileType('r'),
                           help='Database password file')
    parser.add_argument('--site-name', help='Site name (e.g. mhs-testloop-atlanta)')
    parser.add_argument('--table-name', help='Table name to look for data')
    parser.add_argument('--index-field', default='id', help='Index field')
    parser.add_argument('--order-by-field', default='timestamp', help='Field to order data by')
    parser.add_argument('--batch-size', type=int, default=1000, help='How many rows to send at once')
    parser.add_argument('--cloud-url', help='Where to send data to')
    parser.add_argument('--send-interval', type=int, default=60, help='How often to send data (in seconds)')
    parser.add_argument('--error-delay', type=int, default=1, help='How long to wait before next attempt after '
                                                                   'failure (in seconds)')
    parser.add_argument('--leftover-delay', type=int, default=1, help='How long to wait before next send in case '
                                                                      'of more data (in seconds)')
    parser.add_argument('--log-level', default=logging.INFO, choices=logging._levelToName.values(),
                        help='Logging level')

    args = vars(parser.parse_args())

    logging.basicConfig(level=args.pop('log_level'), format=LOGGING_FORMAT)

    sender = Sender(**args)
    sender.start()
