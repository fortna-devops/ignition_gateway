#!/usr/bin/env python

import logging
import signal
import random
import operator
import time
from typing import Callable, Tuple, List, ClassVar

import configargparse
from pyModbusTCP.server import ModbusServer, DataBank


logger = logging.getLogger()

LOGGING_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'


class DeviceSimulator:
    operations: ClassVar[Tuple[Callable[[int, int], int], ...]] = (operator.add, operator.sub)

    address: ClassVar[int]
    base_values: ClassVar[List[int]]

    @classmethod
    def get_values(cls):
        res = []
        for v in cls.base_values:
            res.append(random.choice(cls.operations)(v, random.randint(0, 5)))
        return res


class QM42VT2DeviceSimulator(DeviceSimulator):
    address = 5200
    base_values = [47, 119, 8307, 2837, 56, 142, 49, 38, 97, 97, 7, 7, 3076, 2936, 4848, 3843, 66, 169, 79, 202, 10, 9]


def _signal_handler(signum, _frame):
    logger.debug(f'Got signal {signum}')
    server.stop()


for sig in (signal.SIGINT, signal.SIGTERM):
    signal.signal(sig, _signal_handler)


if __name__ == '__main__':
    parser = configargparse.ArgumentParser(default_config_files=['config.ini'], auto_env_var_prefix='modbus_emulation_')
    parser.add_argument('--host', default='0.0.0.0', help='Where to listen for requests')
    parser.add_argument('--port', type=int, default=502, help='Port to listen on')
    parser.add_argument('--update-time', type=int, default=1, help='How often to update values (in seconds)')
    parser.add_argument('--log-level', default=logging.INFO, choices=logging._levelToName.values(),
                        help='Logging level')

    args = parser.parse_args()

    logging.basicConfig(level=args.log_level, format=LOGGING_FORMAT)

    server = ModbusServer(host=args.host, port=args.port, no_block=True)
    server.start()

    last_update = 0
    while True:
        now = time.time()
        if now - last_update > args.update_time:
            DataBank.set_words(QM42VT2DeviceSimulator.address, QM42VT2DeviceSimulator.get_values())
            last_update = now
        time.sleep(.01)
